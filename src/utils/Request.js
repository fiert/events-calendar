import axios from 'axios';

export const request = axios.create({
  baseURL: 'https://www.googleapis.com/calendar/v3',
  timeout: 10000,
});
