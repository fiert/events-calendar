import { request } from './Request';
import { API } from '../config/config.json';

class CalendarAPI {
  constructor() {
    this.accessToken = localStorage.getItem('accessToken');
  }

  async getFirstCalendarId() {
    const response = await request.get(`${API.CALENDARS_LIST}?access_token=${this.accessToken}&minAccessRole=owner`);
    return response.data.items[0].id;
  }

  async getCalendarEvents() {
    const calendarId = await this.getFirstCalendarId();
    const response = await request.get(`${API.EVENTS_LIST}/${calendarId}/events?access_token=${this.accessToken}&showDeleted=false&singleEvents=true`);
    return response.data.items;
  }

  async getColors() {
    return await request.get(`${API.COLORS_LIST}?access_token=${this.accessToken}`);
  }

  async createEvent(start, end, summary, description) {
    const calendarId = await this.getFirstCalendarId();
    await request.post(`${API.EVENTS_LIST}/${calendarId}/events?access_token=${this.accessToken}`, {
      start: { dateTime: start },
      end: { dateTime: end },
      summary,
      description,
      transparency: 'opaque',
      colorId: 7
    });
  }

  async updateEvent(eventId, start, end, summary, description) {
    const calendarId = await this.getFirstCalendarId();
    await request.put(`${API.EVENTS_LIST}/${calendarId}/events/${eventId}?access_token=${this.accessToken}`, {
      start: { dateTime: start },
      end: { dateTime: end },
      summary,
      description,
      transparency: 'opaque',
      colorId: 7
    });
  }

  async deleteEvent(eventId) {
    const calendarId = await this.getFirstCalendarId();
    await request.delete(`${API.EVENTS_LIST}/${calendarId}/events/${eventId}?access_token=${this.accessToken}`);
  }
}

export const calendarApi = new CalendarAPI();
