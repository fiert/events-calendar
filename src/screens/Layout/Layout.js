import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Route, Switch, Redirect } from 'react-router-dom';
import { layoutStyles as styles } from './LayoutStyles';
import Navigation from '../../components/Navigation/Navigation';
import Login from '../Login/Login';
import Calendar from '../Calendar/Calendar';

const accessToken = localStorage.getItem('accessToken');

class Layout extends Component {
  render() {
    return (
      <div>
        <Navigation/>
        <div>
          <Switch>
            <Route path={'/login'} component={Login}/>
            <Route path={'/calendar'} component={Calendar}/>
            <Redirect to={accessToken ? '/calendar' : '/login'}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Layout);
