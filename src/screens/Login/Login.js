import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import GoogleLogin from 'react-google-login';
import { loginStyles as styles } from './LoginStyles';
import { google } from '../../config/config';
import history from '../../history';

class Login extends Component {

  onSuccessLogin = ({ accessToken, googleId }) => {
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('googleId', googleId);
    return history.push('/calendar');
  };

  onFailureLogin = (err) => {
    console.log(err);
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <div className={classes.title}>Login</div>
        <GoogleLogin
          clientId={google.clientID}
          buttonText="Login"
          scope={'https://www.googleapis.com/auth/calendar'}
          onSuccess={this.onSuccessLogin}
          onFailure={this.onFailureLogin}
          className={classes.authButton}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Login);
