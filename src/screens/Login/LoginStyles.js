export const loginStyles = {
  container: {
    width: 400,
    height: 300,
    background: '#f3f3f3',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    boxShadow: '0px 2px 8px 0px',
  },
  title: {
    textAlign: 'center',
    padding: '15px 0',
    fontSize: 24,
    color: '#6a6a6a',
    borderBottom: '1px solid'
  },
  authButton: {
    width: 350,
    textAlign: 'center',
    margin: '50px 25px',
    paddingLeft: '120px !important',
    paddingRight: '120px !important'
  }
};
