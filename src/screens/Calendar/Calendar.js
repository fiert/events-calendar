import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { calendarStyles as styles } from './CalendarStyles';
import { Button, Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import EventCalendar from '../../components/EventCalendar/EventCalendar';
import { calendarApi } from '../../utils/CalendarAPI';

import EventForm from '../../components/EventForm/EventForm';

const transformEventsResponse = (events) => {
  return events.map((e) => ({
    id: e.id,
    start: new Date(e.start.dateTime),
    end: new Date(e.end.dateTime),
    title: e.summary,
    colorId: e.colorId
  }));
};

class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      calendarId: '',
      events: [],
      openForm: false,
      eventColors: [],
      selected: {}
    };
  }

  componentDidMount() {
    calendarApi.getCalendarEvents().then((events) => this.setState({ events: transformEventsResponse(events) }));
    calendarApi.getColors().then((colors) => this.setState({ eventColors: colors.data.event }));
  }

  render() {
    const { events, openForm, eventColors, selected } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <Button
          className={classes.button}
          color={'primary'}
          variant={'contained'}
          onClick={() => this.setState({ openForm: true, selected: {} })}
        >
          New Event
        </Button>
        <EventCalendar events={events} eventColors={eventColors} onSelectEvent={this.onSelectEvent}/>
        <Dialog open={openForm}>
          <DialogTitle>{selected.start ? 'Edit Event' : 'Create new Event'}</DialogTitle>
          <DialogContent>
            <EventForm
              data={selected}
              onCancelClick={() => this.setState({ openForm: false })}
              onCreateClick={this.onCreateClick}
              onUpdateClick={this.onUpdateClick}
              onDeleteClick={this.onDeleteClick}
            />
          </DialogContent>
        </Dialog>
      </div>
    );
  }

  onSelectEvent = async (event) => {
    this.setState({ selected: event, openForm: true });
  };

  onCreateClick = async (start, end, title, description) => {
    await calendarApi.createEvent(start, end, title, description);
    this.setDelayAfterAction();
  };

  onUpdateClick = async (eventId, start, end, title, description) => {
    await calendarApi.updateEvent(eventId, start, end, title, description);
    this.setDelayAfterAction();
  };

  onDeleteClick = async (eventId) => {
    await calendarApi.deleteEvent(eventId);
    this.setDelayAfterAction();
  };

  setDelayAfterAction = () => {
    return setTimeout(() => {
      this.setState({ openForm: false, selected: {} });
      return calendarApi.getCalendarEvents().then((events) => this.setState({ events: transformEventsResponse(events) }));
    }, 1000);
  }
}

export default withStyles(styles)(Calendar);
