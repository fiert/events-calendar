import React, { Component } from 'react';
import moment from 'moment/moment';
import { withStyles } from '@material-ui/core/styles/index';
import Calendar from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { eventCalendarStyles as styles } from './EventCalendarStyles';

const localizer = Calendar.momentLocalizer(moment);

class EventCalendar extends Component {
  render() {
    const { classes, events, eventColors, onSelectEvent } = this.props;
    return (
      <Calendar
        localizer={localizer}
        defaultDate={new Date()}
        defaultView={'month'}
        events={events}
        style={{ height: '100vh' }}
        classNames={classes.calendar}
        onSelectEvent={onSelectEvent}
        eventPropGetter={(event) => {
          return ({
            style: {
              ...eventColors[event.colorId]
            }
          });
        }}
      />
    );
  }
}

export default withStyles(styles)(EventCalendar);
