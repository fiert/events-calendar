import React, { Component, Fragment } from 'react';
import moment from 'moment';
import { withStyles } from '@material-ui/core/styles';
import { TextField, Grid, Button } from '@material-ui/core';
import { DateTimePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';

import { eventFormStyles as styles } from './EventFormStyles';

class EventForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventId: props.data.id || '',
      title: props.data.title || '',
      start: props.data.start || moment(),
      end: props.data.end || moment().add(1, 'day'),
      description: props.data.description || ''
    };
  }

  render() {
    const { eventId, title, start, end, description } = this.state;
    const { classes, onCancelClick, onCreateClick, onUpdateClick, onDeleteClick, data } = this.props;
    return (
      <div className={classes.container}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <TextField
              label={'Title'}
              variant={'outlined'}
              value={title}
              className={classes.textField}
              fullWidth={true}
              onChange={this.onInputChange('title')}
            />
          </Grid>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <Grid item xs={6}>
              <DateTimePicker
                label={'Date Start'}
                variant={'outlined'}
                value={start}
                onChange={this.onInputChange('start')}
              />
            </Grid>
            <Grid item xs={6}>
              <DateTimePicker
                label={'Date End'}
                variant={'outlined'}
                value={end}
                onChange={this.onInputChange('end')}
              />
            </Grid>
          </MuiPickersUtilsProvider>
          <Grid item xs={12}>
            <TextField
              variant={'outlined'}
              label={'Description'}
              fullWidth={true}
              value={description}
              multiline
              rows="4"
              className={classes.textField}
              onChange={this.onInputChange('description')}
            />
          </Grid>
        </Grid>
        <div className={classes.buttonContainer}>
          <Button
            variant={'outlined'}
            className={classes.button}
            onClick={onCancelClick}
          >
            Cancel
          </Button>
          {data.start ?
            <Fragment>
              <Button
                color={'secondary'}
                variant={'contained'}
                className={classes.button}
                onClick={() => onDeleteClick(eventId)}
              >
                Delete
              </Button>
              <Button
                color={'primary'}
                variant={'contained'}
                className={classes.button}
                onClick={() => onUpdateClick(eventId, start, end, title, description)}
              >
                Update
              </Button>
            </Fragment> :
            <Button
              color={'primary'}
              variant={'contained'}
              className={classes.button}
              onClick={() => onCreateClick(start, end, title, description)}
            >
              Create
            </Button>
          }
        </div>
      </div>
    );
  }

  onInputChange = name => (e) => {
    this.setState({ [name]: e.target ? e.target.value : e });
  };
}

export default withStyles(styles)(EventForm);
