export const eventFormStyles = {
  container: {
    width: 450,
    height: 340,
    marginTop: 10
  },
  textField: {
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    margin: 10,
    marginRight: 0
  }
};
