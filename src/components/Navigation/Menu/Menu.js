import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import { menuStyles as styles } from './MenuStyles';
import history from '../../../history';

const Menu = ({ classes, navigationMenu }) => {
  return (
    navigationMenu.map((name, k) =>
      <Button
        key={k}
        onClick={() => history.push(name.toLowerCase())}
        className={classes.container}
      >
        {name}
      </Button>
    )
  );
};

export default withStyles(styles)(Menu);
