export const navigationStyles = {
  container: {
    height: 70,
    width: '100%',
    background: '#ebebeb',
    boxShadow: '0px 2px 8px 0px',
    display: 'flex',
    justifyContent: 'center',
  }
};
