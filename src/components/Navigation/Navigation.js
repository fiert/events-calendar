import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { navigationStyles as styles } from './NavigationStyles';
import Menu from './Menu/Menu';

class Navigation extends Component {
  render() {
    const navigationMenu = [ 'Login', 'Calendar' ];
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <Menu navigationMenu={navigationMenu}/>
      </div>
    );
  }
}

export default withStyles(styles)(Navigation);
