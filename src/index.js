import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Router, Route, Switch } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import App from './App';
import history from './history';
import theme from './theme';

import * as serviceWorker from './serviceWorker';

const accessToken = localStorage.getItem('accessToken');
if (!accessToken) {
  history.push('/login');
}

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Router history={history}>
      <Switch>
        <Route component={App}/>
      </Switch>
    </Router>
  </MuiThemeProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();
